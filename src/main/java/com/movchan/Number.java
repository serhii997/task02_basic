package com.movchan;

/**
 * <h1>Number</h1>
 * The Number program implements an application that
 * simply displays work with number (output range of numbers,
 * search odd and even number
 * to the standard output.
 *
 *
 * @author  Serhii Movchan
 * @version 1.0
 * @since   2019-11-08
 */

public class Number {
    /** field first number */
    private int firstNumber;
    /** field last number */
    private int lastNumber;

    /**
     * This constructor for input two Integer. This is
     * a the simplest form of a class constructor.
     * @param firstNumber This is the first number
     * @param lastNumber  This is the last number
     */

    public Number(int firstNumber, int lastNumber) {
        this.firstNumber = firstNumber;
        this.lastNumber = lastNumber;
    }

    /**
     * This method is output range of numbers. This is
     * a the simplest form of a class method.
     */
    public void outputInterval(){
        for (int i = firstNumber; i <= lastNumber; i++) {
            System.out.print(i + " ");
        }
        System.out.println();
    }

    /**
     * This method is output only odd numbers in range.
     */

    public void outputOddNumber(){
        for (int i = firstNumber; i <= lastNumber; i++) {
            if(i % 2 == 0){
                System.out.print(i + " ");
            }
        }
        System.out.println();
    }

    /**
     * This method is output only even numbers in range.
     */

    public void outputEvenNumber(){
        for (int i = lastNumber; i >= firstNumber; i--) {
            if(i % 2 != 0){
                System.out.print(i + " ");
            }
        }
        System.out.println();
    }

    /**
     * This method is search and output sum odd and even in range.
     */

    public void sumNumbers(){
        int oddSum = 0;
        int evenSum = 0;
        for (int i = firstNumber; i <= lastNumber; i++) {
            if(i % 2 == 0){
                oddSum += i;
            }else{
                evenSum += i;
            }
        }
        System.out.println("Odd number = " + oddSum + " Even number = "+evenSum);
    }
}
