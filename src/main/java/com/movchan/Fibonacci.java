package com.movchan;

import java.util.ArrayList;

/**
 * <h1>Fibonacci.</h1>
 * The Fibonacci program implements an application that
 * simply displays work with Fibonacci number.
 * Search Fibonacci numbers and count percent odd and even numbers for range.
 *
 * @author  Serhii Movchan
 * @version 1.0
 * @since   2019-11-08
 */

public class Fibonacci {
    /** field first Fibonacci number.*/
    private int f1;
    /** field second Fibonacci number. */
    private int f2;
    /** field Fibonacci number. */
    private int numberFibonacci;
    /** field count Fibonacci number.  */
    private int n;
    /** field mass Fibonacci number number.  */
    private ArrayList<Integer> number;

    /**
     * This constructor for input one Integer for search Fibonacci number.
     * This is a the simplest form of a class constructor.
     * @param n This is the count Fibonacci number.
     */

    public Fibonacci(int n) {
        f1 = 1;
        f2 = 1;
        numberFibonacci = 0;
        number = new ArrayList<>();
        this.n = n;
    }

    /**
     * This method is the search and output of Fibonacci numbers on range.
     * This is a the simplest form of a class method.
     */

    public void outputFibonacci() {
        for (int i = 0; i < n; i++) {
            numberFibonacci = f1 + f2;
            f2 = f1;
            f1 = numberFibonacci;
            System.out.print(numberFibonacci + " ");
            number.add(numberFibonacci);
        }
        System.out.println();
    }

    /**
     * This method is the search and output percent odd and even range of numbers.
     * This is a the simplest form of a class method.
     */

    public void outputPercent() {
        double count = 0;
        for (int i = 0; i < number.size(); i++) {
            if(number.get(i) % 2 == 0){
                count++;
            }
        }
        double oddPercent = count / n;
        double evenPercent = (n - count) / n;
        System.out.println("odd percent = "+oddPercent);
        System.out.println("even percent = "+evenPercent);
    }
}
