package com.movchan;

import java.util.Scanner;

/**
 * The Main program implements an application that
 * simply displays Number and Fibonacci programs.
 *
 * @author  Serhii Movchan
 * @version 1.0
 * @since   2019-11-08
 */

public class Main {

    public static void main(String[] args) {
        /** this library helped us input value in console application */
        Scanner sc = new Scanner(System.in);

        System.out.println("Pleas range number");
        int firstNumber = sc.nextInt();
        int lastNumber = sc.nextInt();
        /** this class causes Number function */
        Number number = new Number(firstNumber, lastNumber);

        number.outputInterval();
        number.outputOddNumber();
        number.outputEvenNumber();
        number.sumNumbers();

        System.out.println("Pleas count Fibonacci numbers");
        int count = sc.nextInt();
        /** this class causes Fibonacci function */
        Fibonacci fibonacci = new Fibonacci(count);
        fibonacci.outputFibonacci();
        fibonacci.outputPercent();


    }
}
